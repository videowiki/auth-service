const BaseServiceV2 = require('../BaseServiceV2');
const ORGANIZATION_SERVICE_API_ROOT = process.env.ORGANIZATION_SERVICE_API_ROOT

class OrganizationService extends BaseServiceV2 {
    constructor() {
        super(ORGANIZATION_SERVICE_API_ROOT);
    }

}

module.exports = new OrganizationService();