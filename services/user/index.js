const BaseServiceV2 = require('../BaseServiceV2');
const USER_SERVICE_API_ROOT = process.env.USER_SERVICE_API_ROOT;

class UserService extends BaseServiceV2 {
    constructor() {
        super(USER_SERVICE_API_ROOT);
    }
    
    getUserByEmail(email) {
        return new Promise((resolve, reject) => {
            this.request.get(USER_SERVICE_API_ROOT + '/by_email').query({ email })
            .then(res => {
                resolve(res.body)
            })
            .catch(reject);
        })
    }
}

module.exports = new UserService();
