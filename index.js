
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})
const upload = multer({ storage: storage })


const controller = require('./controller');
const { server, app } = require('./generateServer')();

app.get('/health', (req, res) => {
    if (!process.env.SECRET_STRING) {
        console.log('SECRET_STRING environment variable is not set');
        return res.status(503).send('SECRET_STRING environment variable is not set');
    }
    return res.status(200).send('OK');
})

app.post("/register", upload.any(), controller.registerUser);
app.post("/login", controller.loginUser);
app.post("/resetPassword", controller.resetPassword);

app.post('/generateLoginToken', controller.generateLoginToken)
app.post('/refreshToken', controller.refreshToken)
app.post('/decodeToken', controller.decodeToken)

app.post('/encryptPassword', controller.encryptPassword);

const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
